import { StyleSheet } from 'react-native';


export default StyleSheet.create({
    container: {
      flex: 1,  
    },

    item: {
      padding: 30,
      borderBottomWidth: 10,
      borderBottomColor: '#ccc',
      backgroundColor:'white',
      borderColor:'red',
    },
  
    title:{
      fontWeight: 'bold',
      marginLeft:2,
    },

    description:{
       textAlign:'left',
       margin:5,
       marginRight:5
    },

  });