export const fetchPost = (id) => {
 
  return {
    type: 'FETCH_POST',
    payload: id
  };
}