import React from 'react';

import { Router, Scene } from 'react-native-router-flux';
import { StyleSheet  } from 'react-native';

import RepoList from '../app/components/RepoList';
import PostDetail from '../app/components/PostDetail';
import AppTitle from './components/appTitle';




  export default RouterComponent = () => {
  return (
    <Router>
    
    <Scene key="root">
      
      <Scene key="repoList" 
             component={RepoList} 
             navigationBarStyle={styles.navBar} 
             titleStyle={styles.navTitle} 
             initial 
             renderTitle={() => { return <AppTitle title='Anúncios'/>; }}/>
      
      
      <Scene key="postDetail" 
             component={PostDetail} 
             navigationBarStyle={styles.navBar} 
             titleStyle={styles.navTitle} 
             renderTitle={() => { return <AppTitle title='Informações'/>; }}
             leftButtonImage ={{color:'white'}}
             headerTintColor= 'white'
             />
     </Scene>
    
    </Router>
  );
};


const styles = StyleSheet.create({
    navBar: {
      backgroundColor: 'rgb(35,104,255)', 
      color:'white'
    },
    navTitle: {
      color: 'white',
      justifyContent: 'center',
      alignItems:'center',
      textAlign:'center'
    },

})




