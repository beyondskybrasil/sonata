import React, { Component } from 'react';
import { StyleSheet } from 'react-native';

import CardInfo from '../components/cardInfo';




export default class PostDetail extends Component {
  

 

  render() {
   
     
    const { title, description, amount , price , picture  } = this.props.item ;
   

    return (
      
            <CardInfo title ={title} 
                      price ={price} 
                      description ={description }
                      amount ={amount}
                      picture ={picture}
                      />
      
           );
  }
}


const styles = StyleSheet.create({
  titleStyle: {
    fontSize: 20,
    fontWeight: 'bold'
  },
  bodyStyle: {
    fontSize: 16,
  }
});

