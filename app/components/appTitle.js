import React from 'react';
import { Dimensions, 
         View, 
         Image, 
         Text,
         StyleSheet,
         } from 'react-native';

  

let width = Dimensions.get('window').width * 0.07;
let height = Dimensions.get('window').height * 0.07;


export default AppTitle = ({title}) => {
 
  

    return (
      <View style={styles.container}>
          <Image source={require('../images/sonata.jpg')}
                style={styles.picture} 
           />
           <Text style={styles.title}> { title }</Text>
      </View>
    );
  };


  const styles = StyleSheet.create({
    container: {
      flex:6,
      justifyContent:'center', 
      alignItems: 'center', 
      marginTop: 1, 
      marginLeft:3, 
      flexDirection:'row',
    },

    title: {
      color:'white',
      fontWeight: "bold",
      fontSize:20, 
      alignItems: 'center'
    },
  
    picture: {
      width: width , 
      height: height ,
      margin:2,
    },

})
