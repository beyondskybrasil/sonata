import React, { Component } from 'react';
import { Dimensions, 
    View, 
    Image, 
    Text,
    StyleSheet,
    ScrollView
    } from 'react-native';


    let width = Dimensions.get('window').width * 0.9;
    let height = Dimensions.get('window').height * 0.4;


export default CardInfo = ({title , description, amount , price , picture }) => {
 
    const full = <Text style={styles.bold}>Quantidade: {amount} </Text>;
    const empty = <Text style={styles.bold}> Sem estoque! </Text>;
    
  

    return (
        <ScrollView style={styles.container} >
                <View style={{ justifyContent:'center', alignItems: 'center', marginBottom:5}}>
                      <Text style={styles.title}>{title}</Text>
                </View>
                 
                
                <View style={{ justifyContent:'center', alignItems: 'center',}}>  
                       <Image
                               style={{width: width, height: height, borderRadius:20}}
                               source={picture.length ?
                                      {uri: picture}  :  require('../images/not_found.png')}
                        />
                </View> 
         
         
                <View>
                       <Text style={styles.body}> <Text style={styles.bold}> Descrição: </Text> {'\n'}
                       {'        '}{description} </Text>
                </View>



                    
                      <View style={{flexDirection:'row', justifyContent:'space-between', marginTop:5}}>
                      
                      <Text style={styles.bold}> Preço: R${price} </Text>
                      { amount+''.length ?  full : empty  }
                      
                      </View>
        
         
         
         
         
         </ScrollView > 
    );
  };


  const styles = StyleSheet.create({
    container: {
        borderColor:'rgb(124, 124, 116)', 
        backgroundColor:'rgba(247,238,158,0.8)', 
        borderWidth:1,
        margin:5, 
        elevation:3, 
        borderRadius:10,
    },

    title: {
       color:'black',
       fontWeight: "bold",
       fontSize:20, 
       marginLeft:5,

    },
  
    body: {
       color:'black',
       fontSize:15, 
       alignItems: 'flex-start',
       marginLeft:10,
       marginTop:3
        
      },

    picture: {
      width: width , 
      height: height ,
      margin:2,
    },

    bold:{
      fontWeight:'bold',
      fontSize: 18,
      margin:10
    },



})