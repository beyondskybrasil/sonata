import React, { Component } from 'react';

import { View, 
         Text, 
         FlatList,
         TouchableOpacity,
         Image } from 'react-native';

import { connect } from 'react-redux';
import { listRepos } from '../reducers/reducer';
import { Actions } from 'react-native-router-flux';

import styles from '../styles/Repolist.component.style.js'


class RepoList extends Component {

  componentDidMount() {
    this.props.listRepos('2');
  }

  _onPress = (item) => {
      Actions.postDetail({ item: item });
  };
  
  constructor(props) {
    super(props)
   
      
  }
  
  handleImageErrored() {
    this.setState({ imageStatus: "failed to load" });
  }
  
  
  renderItem = ({ item }) => (
    

 <TouchableOpacity onPress={() =>  this._onPress(item)}>
       <View style={styles.item}>
               <View style={{flexDirection:'column'}}>
                    <Text style={styles.title}>{item.title}</Text>
                       <View style={{flexDirection:'row', padding:5}}>
                            <Image
                               style={{width: 50, height: 50}}
                               source={item.picture.length ?
                                      {uri: item.picture}  :  require('../images/not_found.png')}
                             />
                             
                             <Text style={styles.description}>{item.description}</Text>
                      
                      
                        </View>

               </View>
       </View>
 </TouchableOpacity>


                            );

  
  
  render() {
    const { repos } = this.props;
    return (
      <FlatList
      keyExtractor = { (item, index) => index.toString() }
        styles={styles.container}
        data={repos}
        renderItem={this.renderItem}
       />
          );
          }
}



const mapStateToProps = state => {
  let storedRepositories = state.repos.map(repo => ({ key: repo.id, ...repo }));
  return {
    repos: storedRepositories
  };
};

const mapDispatchToProps = {
  listRepos
};





export default connect(mapStateToProps, mapDispatchToProps)(RepoList);